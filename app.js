//20240115
//code work well with PowerApp, Gene
const express = require('express');
const QRCode = require('qrcode');
const app = express();
const PORT = process.env.PORT || 3000;

// Endpoint to generate QR code
app.get('/api/generate', async (req, res) => {
  try {
    const data = req.query.data || 'Hello, World!'; // Default data if not provided
    const qrCode = await QRCode.toBuffer(data, { type: 'png' });

    // Set content type to image/png
    res.header('Content-Type', 'image/png');
    
    // Send raw image data
    res.send(qrCode);
  } catch (error) {
    console.error('Error generating QR code:', error);
    res.status(500).send('Internal Server Error');
  }
});

// Start the server
app.listen(PORT, () => {
  console.log(`Server is running at http://localhost:${PORT}`);
});
